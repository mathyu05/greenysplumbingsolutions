<style>
<?php
    include("contactContent.css");
?>
</style>

<div class="container">
    <div class="row d-flex justify-content-center align-items-center pageTitle">
        <div class="pageTitleOverlay d-flex justify-content-center align-items-center mainTransparentBackground">
            <h1>Contact</h1>
        </div>
    </div>
    <div class="row pageContent">
        <div class="col-12 d-flex align-items-center topBottom10pxMargin serviceAreaTitle">
            We serve the entire Brisbane area!  For excellent, wide-ranging service, 
            and top quality workmanship, choose Greeny's Plumbing Solutions today.
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end contactCallNow">
            <?php include("callNow.php"); ?>
        </div>
        <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start contactFacebook">
            <?php include("findUsOnFacebook.php"); ?>
        </div>
    </div>
    <div class="row topBottom10pxMargin serviceArea">
    </div>
</div>