<style>
    <?php
        include("indexServices.css");
    ?>
</style>

<div class="servicesContainer">

    <div class="row topBottom10pxMargin services">
        <div class="col-12 col-md-4">
            <div class="row">
                <div class="col-4 col-md-12 serviceImage serviceOneImage">
                </div>
                <div class="col-8 col-md-12 text-center serviceContent serviceOneContent">
                    <h2>Great Service</h2>
                    <p>Wide range of services offered for both residential and commercial customers</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="row d-flex flex-row-reverse ">
                <div class="col-4 col-md-12 serviceImage serviceTwoImage">
                </div>
                <div class="col-8 col-md-12 text-center serviceContent serviceTwoContent">
                    <h2>Local Business</h2>
                    <p>A small Brisbane owned and operated business</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="row">
                <div class="col-4 col-md-12 serviceImage serviceThreeImage">
                </div>
                <div class="col-8 col-md-12 text-center serviceContent serviceThreeContent">
                    <h2>Quality Solutions</h2>
                    <p>Whatever the job, you can be sure you're getting the absolute highest quality</p>
                </div>
            </div>
        </div>
    </div>

</div>