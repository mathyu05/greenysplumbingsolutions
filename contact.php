<!DOCTYPE html>
<html lang="en">
<head>
    <?php include("globalHead.php"); ?>
</head>
<body class="container mainBackground">

    <?php include("header.php"); ?>

    <?php include("contactContent.php"); ?>

    <?php include("footer.php"); ?>

</body>
</html>