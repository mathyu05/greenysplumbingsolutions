<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<title>Greeny's Plumbing Solutions</title>
<link rel="icon" href="image/navLogo.png">

<style>
    <?php 
        include("lib/bootstrap/css/bootstrap-reboot.min.css");
        include("lib/bootstrap/css/bootstrap.min.css");
        
        include("generics.css");
    ?>
</style>

<script type="text/javascript" src="lib/jquery/jquery-3.2.1.js"></script>
<script type="text/javascript" src="lib/popperjs/popper.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>

<link href="https://fonts.googleapis.com/css?family=Limelight|Lobster" rel="stylesheet">