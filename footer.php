<style>
    <?php 
        include("footer.css");
    ?>
</style>


<footer class="footerBackground">

    <div class="row">

        <div class="col-12">
            <div class="row footerContent">
                <div class="col-4 d-flex justify-content-center footerLinks">
                    <?php include("navBarPageList.php"); ?>
                </div>
                <div class="col-8">
                    <div class="row d-flex justify-content-center justify-content-md-start footerCall">
                        <?php include("callNow.php"); ?>
                    </div>
                    <div class="row d-flex justify-content-center justify-content-md-start footerSocialMedia">
                        <?php include("findUsOnFacebook.php"); ?>
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center footerInformation">
                Greeny's Plumbing Solutions<br>ABN: 89 870 121 244<br>QBCC: 15 005 747
            </div>
        </div>
   

</div>
</footer>