<style>
<?php
    include("servicesContent.css");
?>
</style>

<div class="container">
    <div class="row d-flex justify-content-center align-items-center pageTitle">
        <div class="pageTitleOverlay d-flex justify-content-center align-items-center mainTransparentBackground">
            <h1>Services</h1>
        </div>
    </div>
    <div class="row d-flex align-items-center topBottom10pxMargin pageContent">
        <div class="col-md-6 col-12">
            <h2>Plumbing</h2>
            <p>All commercial and residential plumbing services.</p>
        </div>
        <div class="col-md-6 col-12">
            <h2>Gas Fitting</h2>
            <p>All gas fitting services.</p>
        </div>
        <div class="col-md-6 col-12">
            <h2>Drainage</h2>
            <p>All drainage services, including investigating and clearing blocked drains.</p>
        </div>
        <div class="col-md-6 col-12">
            <h2>Roofing</h2>
            <p>Roofing Services.</p>
        </div>
        <div class="col-12 moreInformation">
            <div>
                <h2>More Information</h2>
                <p>For more information on the services we provide and how we can help you, get in touch!.</p>
                <div class="row">
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-end contacts topBottom10pxMargin">
                        <?php include("callNow.php"); ?>
                    </div>
                    <div class="col-md-6 col-12 d-flex justify-content-center justify-content-md-start contacts topBottom10pxMargin">
                        <?php include("findUsOnFacebook.php"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row topBottom10pxMargin servicesGallery">

        <div class="col-md-6 col-12">
            <img class="img-fluid img-thumbnail" src="image/services/01.jpg" alt="">
        </div>
        <div class="col-md-6 col-12">
            <img class="img-fluid img-thumbnail" src="image/services/02.jpg" alt="">
        </div>

    </div>
    
</div>