# Greeny's Plumbing Solutions #

A project for Greeny's Plumbing Solutions.

### Site Map ###

* Global:
    * Common colour scheme:
        * Lots of white space.
        * Shades of green (but not like shrek).
        * Complementary colours?
    * Header:
        * Phone number.
        * navbar.
        * Logo.
    * Footer:
        * ABN.
        * Services.
        * Phone number.
* Home:
    * Feature space.
    * Brief information about services.
* About:
    * About the business.
    * Some photos.
    * Accreditation / ABN.
* Services:
    * Subsections showing each major category.
    * Small paragraph about each one.
* Contact:
    * Operating hours.
    * Service area.
    * Phone number.

### Plan ###

* Branching Strategy:
    * Branch off master to do work.
    * Create Pull Requests to merge back to master.
* Static pages to begin with then move to a more dynamic look.

### Author ###

* Matthew Baldwin (matthew.baldwin@uqconnect.edu.au).