<style>
    <?php 
        include("header.css");
    ?>
</style>

<a href="index.php" class="homeLink">
    <div class="row d-flex align-items-end header">
        <div class="col-12 col-sm-4 col-md-3 text-center">
            <img class="headerLogo" src="image/navLogo.png" alt="Greeny's Plumbing Solutions">
        </div>
        <div class="col-12 col-sm-8 col-md-9 headerText">
            <p class="businessName logoFont">
                Greeny's Plumbing Solutions
            </p>
            <p class="businessSlogan sloganFont">
                All Cisterns Go!
            </p>
        </div>
    </div>
</a>

<nav class="navbar navbar-expand-sm navbar-light bg-faded navbarColor">
    <?php include("callNow.php"); ?>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Links -->
    <div class="collapse navbar-collapse" id="nav-content">
        <?php include("navBarPageList.php"); ?>
    </div>
</nav>