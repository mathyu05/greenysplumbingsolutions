<style>
<?php
    include("aboutContent.css");
?>
</style>

<div class="container">
    <div class="row d-flex justify-content-center align-items-center pageTitle">
        <div class="pageTitleOverlay d-flex justify-content-center align-items-center mainTransparentBackground">
            <h1>About</h1>
        </div>
    </div>
    <div class="row pageContent topBottom10pxMargin">
        <div class="col-12">
            We're a new small plumbing business based in Brisbane looking to give homeowners and
            businesses a comprehensive choice for all your plumbing needs.

            <br><br>

            We strive to bring you the highest quality solutions across all jobs.  Be it emergencies,
            drainage services, roofing work, gas fitting, and facility improvement.  From single day
            jobs to long term projects and everything in between, Greeny's Plumbing Solutions have got
            you covered.

            <br><br>

            Daniel Green (Greeny) is a Brisbane raised, fully qualified plumber with a comprehensive
            range of residential and commercial experience.  Over the years, Greeny has done the small
            jobs and the big jobs, including a stint working in the mines.

            <br><br>

            When you call Greeny you can be proud that you are supporting a local business.
        </div>
    </div>
    <div class="row topBottom10pxMargin aboutGallery">

        <!-- TODO Add image of truck and possibly dan -->
        <div class="col-lg-3 col-md-4 col-12">
            <img class="img-fluid img-thumbnail" src="image/ute.jpg" alt="The Work Truck">
        </div>

    </div>

</div>



<!-- TODO IMAGE SECTION -->
