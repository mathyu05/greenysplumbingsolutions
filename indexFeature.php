<style>
    <?php
        include("indexFeature.css");
    ?>
</style>

<div class="featureContainer">

    <div class="row d-flex justify-content-center align-items-center feature">
        <div class="row featureOverlay d-flex justify-content-center align-items-center mainTransparentBackground">
            <div>
                Whatever your plumbing needs might be...
                
                <br><br>

                Call Greeny!
            </div>
            <div>
                <?php include("callNow.php"); ?>
            </div>
        </div>
    </div>

</div>